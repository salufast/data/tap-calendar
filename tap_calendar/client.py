"""Custom client handling, including calendarStream base class."""
from typing import Any, Iterable, Optional, Union
from singer_sdk.streams import Stream
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import re
from babel.dates import format_date


class calendarStream(Stream):
    """Stream class for calendar streams."""

    def get_replication_key_signpost(
        self, context: Optional[dict]
    ) -> Optional[Union[datetime, Any]]:
        duration = self.config.get("duration")
        pattern = "P((?P<years>[0-9]+)Y)?((?P<months>[0-9]+)M)?((?P<days>[0-9]+)D)?"
        m = re.match(re.compile(pattern), duration)

        years = m.group("years")
        if years is not None:
            years = int(years)
        else:
            years = 0
        months = m.group("months")
        if months is not None:
            months = int(months)
        else:
            months = 0
        days = m.group("days")
        if days is not None:
            days = int(days)
        else:
            days = 0

        return datetime.now() + relativedelta(years=years, months=months, days=days)

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """
        d = self.get_starting_timestamp(context=context).date()

        while d < self.get_replication_key_signpost(context=context).date():
            _, week, weekday = d.isocalendar()
            timetuple = d.timetuple()
            date_key = d.year * 10000 + d.month * 100 + d.day  # yyyymmdd as integer

            yield {
                "date_key": date_key,
                "date": d.isoformat(),
                "next_day": (d + timedelta(days=1)).isoformat(),
                "calendar_quarter": format_date(d, format="QQQ"),
                "calendar_quarter_de": format_date(d, format="QQQQ", locale="de"),
                "day_number_in_calendar_month": timetuple.tm_mday,
                "calendar_month": d.month,
                "calendar_year": d.year,
                "day_number_in_calendar_year": timetuple.tm_yday,
                "week_number_in_calendar_year": week,
                "weekday_indicator": (weekday < 6),
                "full_date_description_de": format_date(d, format="long", locale="de"),
                "calendar_month_de": format_date(d, format="MMMM", locale="de"),
                "weekday_de": format_date(d, format="EEEE", locale="de"),
            }
            d += timedelta(days=1)
