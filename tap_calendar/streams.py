"""Stream type classes for tap-calendar."""
from singer_sdk import typing as th  # JSON Schema typing helpers
from tap_calendar import typing as custom_types

from tap_calendar.client import calendarStream


class DateStream(calendarStream):
    name = "date"
    primary_keys = ["date_key"]
    replication_key = "next_day"

    schema = th.PropertiesList(
        th.Property("date_key", th.IntegerType, required=True),
        th.Property("date", custom_types.FullDateType, required=True),
        th.Property(
            "next_day",
            th.DateTimeType,
            required=True,
            description="Needed as replication bookmark.",
        ),
        th.Property("full_date_description_de", th.StringType, required=True),
        th.Property("calendar_month_de", th.StringType, required=True),
        th.Property("calendar_quarter", th.StringType, required=True),
        th.Property("calendar_quarter_de", th.StringType, required=True),
        th.Property("calendar_month", th.IntegerType, required=True),
        th.Property("calendar_month_de", th.StringType, required=True),
        th.Property("calendar_year", th.IntegerType, required=True),
        th.Property("day_number_in_calendar_month", th.IntegerType, required=True),
        th.Property("day_number_in_calendar_year", th.IntegerType, required=True),
        th.Property("week_number_in_calendar_year", th.IntegerType, required=True),
        th.Property("weekday_indicator", th.BooleanType, required=True),
        th.Property("weekday_de", th.StringType, required=True),
    ).to_dict()
