"""calendar tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_calendar.streams import DateStream

# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [DateStream]


class Tapcalendar(Tap):
    """calendar tap class."""

    name = "tap-calendar"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "duration",
            th.CustomType(
                {"type": "string", "pattern": "P([0-9]+Y)?([0-9]+M)?([0-9]+D)?"}
            ),
            required=True,
            description="Return future dates up to date.today() + duration.\
                        Format: P[m]Y[n]M[o]D denotes a duration of m years, \
                        n months, o days (ISO8601 Duration without time).",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
