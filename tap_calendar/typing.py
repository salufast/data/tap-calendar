from singer_sdk import typing as th  # JSON schema typing helpers

FullDateType = th.CustomType(
    {
        "type": ["string"],
        "format": "full-date",
    }
)
